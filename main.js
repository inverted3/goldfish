const { app, BrowserWindow, globalShortcut, Menu } = require('electron')
const path = require('path')
const url = require('url')
const Store = require('electron-store');
const store = new Store();

let win

function createWindow() {

  let windowPosition = store.get('settings.windowPosition') || undefined;

  win = new BrowserWindow({
    width: 700, 
    height: 55,
    x: windowPosition ? windowPosition.x : 0,
    y: windowPosition ? windowPosition.y : 0,
    frame: false, 
    resizable: false,
    alwaysOnTop: true
  })

  if (!windowPosition) win.center();

  win.loadURL(url.format({
    pathname: path.join(__dirname, 'index.html'),
    protocol: 'file:',
    slashes: true
  }))


  win.on('closed', () => {
    win = null
  })

  win.on('move', () =>{
    store.set('settings.windowPosition', { x: win.getPosition()[0], y: win.getPosition()[1]})
  })

}

app.on('ready', ()=>{
  createWindow()
  const editKey = globalShortcut.register('Alt+Shift+D', () => {
    win.focus()
    win.webContents.executeJavaScript('app.switchEditMode()')
  })

  if (!editKey) {
    console.log('registration failed')
  }

  const historyKey = globalShortcut.register('Alt+Shift+H', () => {
    win.focus()
    win.webContents.executeJavaScript('app.toggleHistory()')
  })

  if (!historyKey) {
    console.log('registration failed')
  }
})

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (win === null) {
    createWindow()
  }
})

app.setAboutPanelOptions({
  applicationName: 'Goldfish',
  credits: "Ohad Ron"
})

app.setName('Goldfish')