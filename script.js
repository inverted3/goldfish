const Vue = require('vue/dist/vue.common');
const { remote } = require('electron');
const moment = require('moment')
const Store = require('electron-store');
const store = new Store();

let globalHotkey = function(){
  app.switchEditMode();
}

Vue.filter('timeago', function(time){
  return moment(time).fromNow()
})

Vue.filter('hour', function (time) {
  if(moment(time).isBefore(moment(time).subtract(moment.duration(1,'days')))){
    return moment(time).format("MMMM Mo - HH:mm")
  }
  return moment(time).format("H:mm")
})

Vue.component('message-displayer', {
  props: ['message'],
  template: `
    <span>
      <span v-if="!message.text" class="main-message" @click="switchMode">Click here to enter a new message</span>
      <span v-if="message.text">
        <div class="main-message" @click="switchMode">{{ message.text }}
        </div>  
        <span class="message-time">{{ message.time | hour }}</span>
      </span>
    </span>  
  `,
  methods: {
    switchMode: function(){
      this.$emit('mode-switch');
    }
  },
})

Vue.component('message-editor', {
  props: ['message'],
  data: function(){
    return { messageInput: this.message.text }
  },
  template: `
    <span>
      <input 
        ref="inputfield" 
        type="text" 
        v-model="messageInput" 
        @keyup.enter="switchMode"
        @keyup.esc="cancelEdit"
        maxlength="60"
        />
      <button @click="switchMode">Change</button>
    </span>  
  `,
  methods: {
    switchMode: function () {
      if (this.messageInput != this.message.text){
        this.$emit('update-message', this.messageInput);
      }
      this.$emit('mode-switch');
    },
    cancelEdit: function(){
      this.$emit('mode-switch');
    }
  },
  mounted: 
    function(){
      this.$refs.inputfield.focus()
      this.$refs.inputfield.select()
    }
})

Vue.component('history-display',{
  props: ['history'],
  template:`
  <div id="history">
    <div v-for="(item, index) in history" class="history-item">
    <span @click="reuseItem(index)">{{item.text}}</span>
    <span @click="reuseItem(index)" class="history-time">
      ({{item.time | hour }})
    </span>
    <button @click="deleteItem(index)">Delete</button>
    </div>
  </div>
  `,
  methods: {
    deleteItem: function (index) {
      this.$emit('delete', index);
    },
    reuseItem: function (index) {
        this.$emit('reuse', index);
    },
  }
})

let app = new Vue({
  el: '#app',
  data:{
    currentMessage: store.get('settings.currentMessage') || {},
    messageHistory: store.get('settings.messageHistory') || [],
    historyShown: store.get('settings.historyShown') || false,
    editMode: false
  },
  methods:{
    switchEditMode: function(){
      this.editMode = !this.editMode
    },
    updateMessage: function(newMessage){
      if (this.currentMessage.text){
        this.messageHistory.unshift(Object.assign({},this.currentMessage))
      }
      this.currentMessage.text = newMessage
      this.currentMessage.time = new Date()
      store.set('settings.currentMessage', this.currentMessage)
      store.set('settings.messageHistory', this.messageHistory)
    },
    deleteHistoryItem: function (index) {
      this.messageHistory.splice(index, 1)
      store.set('settings.messageHistory', this.messageHistory)
    },
    reuseHistoryItem: function (index) {
      let resuedItemText = this.messageHistory[index].text
      this.messageHistory.splice(index, 1)
      store.set('settings.messageHistory', this.messageHistory)
      this.updateMessage(resuedItemText)
    },
    toggleHistory: function(){
      this.historyShown = !this.historyShown
      let currentWindowSize = remote.getCurrentWindow().getSize()
      store.set('settings.historyShown', this.historyShown)
      if(this.historyShown){
        remote.getCurrentWindow().setSize(currentWindowSize[0], currentWindowSize[1]+200, false);
      } else {
        remote.getCurrentWindow().setSize(currentWindowSize[0], currentWindowSize[1]-200, false);
      }
    }
  }
});

// Global keyboard shortcuts
document.onkeydown = function (e) {
  if (e.keyCode == 68 && !app.$data.editMode) {
    e.preventDefault()
    app.switchEditMode()
  }
  if (e.keyCode == 72 && !app.$data.editMode) {
    app.toggleHistory()
  }
};