# Goldfish

I often start working on something but quickly remind myself of a hundred other things that I want to be working on as well, at the same exact time. Goldfish is an experimental app - a window that's (ideally) always visible and always shows you what you were aiming to work on. 

I'm also doing this to learn [Electron](https://electron.atom.io/) and [Vue](https://vuejs.org/).

![](images/goldfish-example.png)

## Usage

When in the app:
- `d` starts editing the current working item
- `h` shows/hides the item history

Globally:
- `Alt + Shift + D` starts editing the current working item
- `Alt + Shift + H` shows/hides the item history

Settings and history are being saved in local storage
Clicking an history item re-selects it as the current working item

## Development

1. Clone the repo
2. `npm install`
3. If you have Electron globally installed you can start the app by just running `Electron .` in the main app folder.

## Goldfish attention span

Goldfish are notorious for having a short attention span. Research shows it's about nine seconds. Humans nowadays have an even shorter one. I hope this app could help goldfish in their effort to reclaim the short attention span throne. 
